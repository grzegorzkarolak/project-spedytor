**PROJEKT SPEDYTOR**  

**Demo aplikacji:**  

[https://grzegorzkarolak.herokuapp.com](https://grzegorzkarolak.herokuapp.com)  
User: user  
Password: password      


[Happy Path (kliknij)](https://bitbucket.org/grzegorzkarolak/project-spedytor/raw/master/spedytor-happy-path-flow.png)  

[Model DB (kliknij)](https://bitbucket.org/grzegorzkarolak/project-spedytor/raw/master/dbmodel.png)  

Aplikacja jest przeznaczona do wspomagania zarządzania firmą transportową/spedycyjną.  

**Użyte technologie**  
:ballot_box_with_check: Java  
:ballot_box_with_check: Spring Boot  
:ballot_box_with_check: Spring MVC  
:ballot_box_with_check: Spring Data  
:ballot_box_with_check: Spring Security  
:ballot_box_with_check: Maven  
:ballot_box_with_check: SQL  
:ballot_box_with_check: Hibernate  
:ballot_box_with_check: Java Bean Validation  
:ballot_box_with_check: Thymeleaf  
:ballot_box_with_check: HTML  
:ballot_box_with_check: JavaScript

**Funkcjonalność:**  
:white_check_mark: Dodawanie, edytowanie, usuwanie komponentów (klientów, przewoźników, aut dostawczych, kierowców, jednostek transportowych, ładunków)  
:white_check_mark: Sprawdzanie poprawności i kompletności wprowadzonych danych  
:white_check_mark: Prezentowanie komponentów  
:white_check_mark: Wyszukiwanie komponentów po zawartości pól  
:white_check_mark: Prezentacja powiązań komponmentów (np. jaka firma zleciła ładunek, jaka firma go przewiozła)  
:white_check_mark: Informacja o flocie danego przewoźnika  

**Kierunek rozwoju:**  
:white_large_square: Generowanie raportów mających wartość biznesową  
:white_large_square: Generowanie zleceń transportowych, autmatyczne wysyłanie  
:white_large_square: Generowanie faktur, automatyczne wysyłanie  
:white_large_square: Przypisanie kierowcy i auta do ładunku  
:white_large_square: Jeden ładunek może posiadać różne jednostki transportowe  
:white_large_square: Wprowadzenie kosztów oraz przychodów za ładunki  
:white_large_square: Powiadomienia o nie zapłaconych fakturach  
:white_large_square: Dodanie typów użytkowników i uprawnień



