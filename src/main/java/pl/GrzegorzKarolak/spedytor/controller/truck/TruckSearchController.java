package pl.GrzegorzKarolak.spedytor.controller.truck;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.GrzegorzKarolak.spedytor.entity.Truck;
import pl.GrzegorzKarolak.spedytor.service.truck.TruckService;

import java.util.List;

@Controller
@RequestMapping("/trucks")
public class TruckSearchController {

    private TruckService truckService;

    @Autowired
    public TruckSearchController(TruckService truckService) {
        this.truckService = truckService;
    }

    @GetMapping("/searchById")
    public String searchById(@ModelAttribute("searchId") String idString, Model model) {
        List<Truck> trucks = null;
        if (!idString.isEmpty()) {
            Integer id = Integer.valueOf(idString);
            trucks = truckService.findByIdAsc(id);
        } else {
            trucks = truckService.findAll();
        }
        model.addAttribute("trucks", trucks);
        return "trucks/trucksList";
    }

    @GetMapping("/searchByTruckReg")
    public String searchByTruckReg(@ModelAttribute("searchTruckReg") String truckReg, Model model) {
        List<Truck> trucks = null;
        if (!truckReg.isEmpty()) {
            trucks = truckService.findByTruckReg(truckReg);
        } else {
            trucks = truckService.findAll();
        }
        model.addAttribute("trucks", trucks);
        return "trucks/trucksList";
    }

    @GetMapping("/searchByLorryReg")
    public String searchByLorryReg(@ModelAttribute("searchLorryReg") String lorryReg, Model model) {
        List<Truck> trucks = null;
        if (!lorryReg.isEmpty()) {
            trucks = truckService.findByLorryReg(lorryReg);
        } else {
            trucks = truckService.findAll();
        }
        model.addAttribute("trucks", trucks);
        return "trucks/trucksList";
    }

    @GetMapping("/searchByKind")
    public String searchByKind(@ModelAttribute("searchKind") String kind, Model model) {
        List<Truck> trucks = null;
        if (!kind.isEmpty()) {
            trucks = truckService.findByKind(kind);
        } else {
            trucks = truckService.findAll();
        }
        model.addAttribute("trucks", trucks);
        return "trucks/trucksList";
    }

    @GetMapping("/searchByCapacity")
    public String searchByCapacity(@ModelAttribute("searchCapacity") String capacityString, Model model) {
        List<Truck> trucks = null;
        if (!capacityString.isEmpty()) {
            Integer capacity = Integer.valueOf(capacityString);
            trucks = truckService.findByCapacity(capacity);
        } else {
            trucks = truckService.findAll();
        }
        model.addAttribute("trucks", trucks);
        return "trucks/trucksList";
    }

    @GetMapping("/searchByDetails")
    public String searchByDetails(@ModelAttribute("searchDetails") String details, Model model) {
        List<Truck> trucks = null;
        if (!details.isEmpty()) {
            trucks = truckService.findByDetails(details);
        } else {
            trucks = truckService.findAll();
        }
        model.addAttribute("trucks", trucks);
        return "trucks/trucksList";
    }

    @GetMapping("/searchByCarrierName")
    public String searchByCarrierName(@ModelAttribute("searchCarrierName") String carrierName, Model model) {
        List<Truck> trucks = null;
        if (!carrierName.isEmpty()) {
            trucks = truckService.findByCarrierName(carrierName);
        } else {
            trucks = truckService.findAll();
        }
        model.addAttribute("trucks", trucks);
        return "trucks/trucksList";
    }
}
