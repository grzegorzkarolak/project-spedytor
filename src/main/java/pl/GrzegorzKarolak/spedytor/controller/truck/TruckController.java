package pl.GrzegorzKarolak.spedytor.controller.truck;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.GrzegorzKarolak.spedytor.entity.Truck;
import pl.GrzegorzKarolak.spedytor.entity.Carrier;
import pl.GrzegorzKarolak.spedytor.service.truck.TruckService;
import pl.GrzegorzKarolak.spedytor.service.carrier.CarrierService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/trucks")
public class TruckController {

    private TruckService truckService;
    private CarrierService carrierService;

    @Autowired
    public TruckController(TruckService truckService, CarrierService carrierService) {
        this.truckService = truckService;
        this.carrierService = carrierService;
    }

    @GetMapping("/list")
    public String listTrucks(Model model) {
        List<Truck> trucks = truckService.findAll();
        model.addAttribute("trucks", trucks);
        return "trucks/trucksList";
    }

    @GetMapping("/retriveTruck")
    public String retriveTruck(Model model) {
        Truck truck = null;
        model.addAttribute("truck", truck);
        return "trucks/trucksList";
    }

    @GetMapping("/showFormForAddTruck")
    public String showFormForUpdateTruck(Model model) {
        Truck truck = new Truck();
        model.addAttribute("truck", truck);

        List<Carrier> carriers = carrierService.findAll();
        model.addAttribute("carriers", carriers);

        List<String> kindOfTrucks = getKindOfTrucks();
        model.addAttribute("kindOfTrucks", kindOfTrucks);

        return "trucks/truckForm";
    }

    @GetMapping("/showFormForUpdateTruck")
    public String showFormForUpdateTruck(@ModelAttribute("truckId") int id, Model model) {
        Truck truck = truckService.findById(id);
        model.addAttribute("truck", truck);

        List<Carrier> carriers = carrierService.findAll();
        model.addAttribute("carriers", carriers);

        List<String> kindOfTrucks = getKindOfTrucks();
        model.addAttribute("kindOfTrucks", kindOfTrucks);

        return "trucks/truckForm";
    }

    @PostMapping("/save")
    public String saveTruck(@Valid @ModelAttribute("truck") Truck truck, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {

            List<Carrier> carriers = carrierService.findAll();
            model.addAttribute("carriers", carriers);

            List<String> kindOfTrucks = getKindOfTrucks();
            model.addAttribute("kindOfTrucks", kindOfTrucks);

            return "trucks/truckForm";
        } else {
            truckService.save(truck);
            return "redirect:list";
        }
    }

    @GetMapping("/delete")
    public String deleteTruck(@ModelAttribute("truckId") int id) {
        truckService.deleteById(id);
        return "redirect:list";
    }

    private static List<String> getKindOfTrucks() {
        List<String> trucks = new ArrayList<>();
        trucks.add("BUS 3.5 T");
        trucks.add("Trailer 13.6, Conestoga");
        trucks.add("Trailer 13.6, IZO/Enclosed");
        trucks.add("Trailer 13.6, Refrigerated");
        trucks.add("Solo/City");
        trucks.add("Tank");
        trucks.add("Platform");
        trucks.add("Other");
        return trucks;
    }
}
