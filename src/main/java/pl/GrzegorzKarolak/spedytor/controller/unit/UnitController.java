package pl.GrzegorzKarolak.spedytor.controller.unit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.GrzegorzKarolak.spedytor.entity.helper.Unit;
import pl.GrzegorzKarolak.spedytor.service.unit.UnitService;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/units")
public class UnitController {

    private UnitService unitService;

    @Autowired
    public UnitController(UnitService unitService) {
        this.unitService = unitService;
    }

    @GetMapping("/list")
    public String listunits (Model model) {
        List<Unit> units = unitService.findAll();
        model.addAttribute("units", units);
        return "units/unitsList";
    }

    @GetMapping ("/retriveUnit")
    public String retriveunit(Model model) {
        Unit unit = null;
        model.addAttribute("unit", unit);
        return "units/unitsList";
    }

    @GetMapping ("/showFormForUpdateUnit")
    public String showFormForUpdateunit(@ModelAttribute ("unitId") int id, Model model) {
        Unit unit = unitService.findById(id);
        model.addAttribute("unit", unit);
        return "units/unitForm";
    }

    @GetMapping ("/showFormForAddUnit")
    public String showFormForUpdateunit(Model model) {
        Unit unit = new Unit();
        model.addAttribute("unit", unit);
        return "units/unitForm";
    }

    @PostMapping("/save")
    public String saveUnit (@Valid @ModelAttribute ("unit") Unit unit, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "units/unitForm";
        } else {
            unitService.save(unit);
            return "redirect:list";
        }
    }

    @GetMapping("/delete")
    public String deleteunit (@ModelAttribute ("unitId") int id) {
        unitService.deleteById(id);
        return "redirect:list";
    }
}
