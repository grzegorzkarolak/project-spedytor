package pl.GrzegorzKarolak.spedytor.controller.unit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.GrzegorzKarolak.spedytor.entity.helper.Unit;
import pl.GrzegorzKarolak.spedytor.service.unit.UnitService;

import java.util.List;

@Controller
@RequestMapping("/units")
public class UnitSearchController {

    private UnitService unitService;

    @Autowired
    public UnitSearchController(UnitService unitService) {
        this.unitService = unitService;
    }

    @GetMapping("/searchById")
    public String searchById(@ModelAttribute("searchId") String idString, Model model) {
        List<Unit> units = null;
        if (!idString.isEmpty()) {
            int id = Integer.parseInt(idString);
            units = unitService.searchById(id);
        } else {
            units = unitService.findAll();
        }
        model.addAttribute("units", units);
        return "units/unitsList";
    }

    @GetMapping("/searchByName")
    public String searchByName(@ModelAttribute("searchName") String name, Model model) {
        List<Unit> units = null;
        if (!name.isEmpty()) {
            units = unitService.searchByName(name);
        } else {
            units = unitService.findAll();
        }
        model.addAttribute("units", units);
        return "units/unitsList";
    }

    @GetMapping("/searchByWidth")
    public String searchByWidth(@ModelAttribute("searchWidth") String widthString, Model model) {
        List<Unit> units = null;
        if (!widthString.isEmpty()) {
            int width = Integer.parseInt(widthString);
            units = unitService.searchByWidth(width);
        } else {
            units = unitService.findAll();
        }
        model.addAttribute("units", units);
        return "units/unitsList";
    }

    @GetMapping("/searchByLength")
    public String searchByLength(@ModelAttribute("searchLength") String lengthString, Model model) {
        List<Unit> units = null;
        if (!lengthString.isEmpty()) {
            int length = Integer.parseInt(lengthString);
            units = unitService.searchByLength(length);
        } else {
            units = unitService.findAll();
        }
        model.addAttribute("units", units);
        return "units/unitsList";
    }

    @GetMapping("/searchByDscription")
    public String searchByDscription(@ModelAttribute("searchDscription") String description, Model model) {
        List<Unit> units = null;
        if (!description.isEmpty()) {
            units = unitService.searchDscription(description);
        } else {
            units = unitService.findAll();
        }
        model.addAttribute("units", units);
        return "units/unitsList";
    }
}


