package pl.GrzegorzKarolak.spedytor.controller.driver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.GrzegorzKarolak.spedytor.entity.Driver;
import pl.GrzegorzKarolak.spedytor.service.driver.DriverService;

import java.util.List;

@Controller
@RequestMapping("/drivers")
public class DriverSearchController {

    private DriverService driverService;

    @Autowired
    public DriverSearchController(DriverService driverService) {
        this.driverService = driverService;
    }

    @GetMapping("/searchById")
    public String searchById(@ModelAttribute("searchId") String idString, Model model) {
        List<Driver> drivers = null;
        if (!idString.isEmpty()) {
            Integer id = Integer.valueOf(idString);
            drivers = driverService.findByIdAsc(id);
        } else {
            drivers = driverService.findAll();
        }
        model.addAttribute("drivers", drivers);
        return "drivers/driversList";
    }

    @GetMapping("/searchByFirstName")
    public String searchByFirstName(@ModelAttribute("searchFirstName") String firstName, Model model) {
        List<Driver> drivers = null;
        if (!firstName.isEmpty()) {
            drivers = driverService.findByFirstName(firstName);
        } else {
            drivers = driverService.findAll();
        }
        model.addAttribute("drivers", drivers);
        return "drivers/driversList";
    }

    @GetMapping("/searchByLastName")
    public String searchByLastName(@ModelAttribute("searchLastName") String lastName, Model model) {
        List<Driver> drivers = null;
        if (!lastName.isEmpty()) {
            drivers = driverService.findByLastName(lastName);
        } else {
            drivers = driverService.findAll();
        }
        model.addAttribute("drivers", drivers);
        return "drivers/driversList";
    }

    @GetMapping("/searchByPhoneNo")
    public String searchByPhoneNo(@ModelAttribute("searchPhoneNo") String phoneNo, Model model) {
        List<Driver> drivers = null;
        if (!phoneNo.isEmpty()) {
            drivers = driverService.findByPhoneNo(phoneNo);
        } else {
            drivers = driverService.findAll();
        }
        model.addAttribute("drivers", drivers);
        return "drivers/driversList";
    }

    @GetMapping("/searchByHaveADR")
    public String searchByHaveADR(@ModelAttribute("searchHaveADR") String haveADRString, Model model) {
        List<Driver> drivers = null;
        if (haveADRString == "Yes") {
            Boolean haveADR = Boolean.valueOf(true);
            drivers = driverService.findByHaveADR(haveADR);
        } else {
            Boolean haveADR = Boolean.valueOf(false);
            drivers = driverService.findByHaveADR(haveADR);
        }
        model.addAttribute("drivers", drivers);
        return "drivers/driversList";
    }

    @GetMapping("/searchByDetails")
    public String searchByDetails(@ModelAttribute("searchDetails") String details, Model model) {
        List<Driver> drivers = null;
        if (!details.isEmpty()) {
            drivers = driverService.findByDetails(details);
        } else {
            drivers = driverService.findAll();
        }
        model.addAttribute("drivers", drivers);
        return "drivers/driversList";
    }

    @GetMapping("/searchByCarrierName")
    public String searchByCarrierName(@ModelAttribute("searchCarrierName") String carrierName, Model model) {
        List<Driver> drivers = null;
        if (!carrierName.isEmpty()) {
            drivers = driverService.findByCarrierName(carrierName);
        } else {
            drivers = driverService.findAll();
        }
        model.addAttribute("drivers", drivers);
        return "drivers/driversList";
    }
}
