package pl.GrzegorzKarolak.spedytor.controller.driver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.GrzegorzKarolak.spedytor.entity.Carrier;
import pl.GrzegorzKarolak.spedytor.entity.Driver;
import pl.GrzegorzKarolak.spedytor.service.carrier.CarrierService;
import pl.GrzegorzKarolak.spedytor.service.driver.DriverService;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/drivers")
public class DriverController {

    private DriverService driverService;
    private CarrierService carrierService;

    @Autowired
    public DriverController(DriverService driverService, CarrierService carrierService) {
        this.driverService = driverService;
        this.carrierService = carrierService;
    }

    @GetMapping("/list")
    public String listDrivers (Model model) {
        List<Driver> drivers = driverService.findAll();
        model.addAttribute("drivers", drivers);
        return "drivers/driversList";
    }

    @GetMapping ("/retriveDriver")
    public String retriveDriver(Model model) {
        Driver driver = null;
        model.addAttribute("driver", driver);
        return "drivers/driversList";
    }

    @GetMapping ("/showFormForAddDriver")
    public String showFormForUpdateDriver(Model model) {
        Driver driver = new Driver();
        model.addAttribute("driver", driver);

        List<Carrier> carriers = carrierService.findAll();
        model.addAttribute("carriers", carriers);

        return "drivers/driverForm";
    }

    @GetMapping ("/showFormForUpdateDriver")
    public String showFormForUpdateDriver(@ModelAttribute ("driverId") int id, Model model) {
        Driver driver = driverService.findById(id);
        model.addAttribute("driver", driver);
        
        List<Carrier> carriers = carrierService.findAll();
        model.addAttribute("carriers", carriers);

        return "drivers/driverForm";
    }

    @PostMapping("/save")
    public String saveDriver (@Valid @ModelAttribute ("driver") Driver driver, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {

            List<Carrier> carriers = carrierService.findAll();
            model.addAttribute("carriers", carriers);

            return "drivers/driverForm";
        } else {
            driverService.save(driver);
            return "redirect:list";
        }
    }

    @GetMapping("/delete")
    public String deleteDriver (@ModelAttribute ("driverId") int id) {
        driverService.deleteById(id);
        return "redirect:list";
    }
}
