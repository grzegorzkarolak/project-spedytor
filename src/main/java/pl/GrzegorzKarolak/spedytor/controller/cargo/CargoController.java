package pl.GrzegorzKarolak.spedytor.controller.cargo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.GrzegorzKarolak.spedytor.entity.Cargo;
import pl.GrzegorzKarolak.spedytor.entity.Carrier;
import pl.GrzegorzKarolak.spedytor.entity.Customer;
import pl.GrzegorzKarolak.spedytor.entity.helper.Unit;
import pl.GrzegorzKarolak.spedytor.service.cargo.CargoService;
import pl.GrzegorzKarolak.spedytor.service.carrier.CarrierService;
import pl.GrzegorzKarolak.spedytor.service.customer.CustomerService;
import pl.GrzegorzKarolak.spedytor.service.unit.UnitService;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/cargos")
public class CargoController {

    private CargoService cargoService;
    private CustomerService customerService;
    private UnitService unitService;
    private CarrierService carrierService;

    @Autowired
    public CargoController(CargoService cargoService, CustomerService customerService, UnitService unitService, CarrierService carrierService) {
        this.cargoService = cargoService;
        this.customerService = customerService;
        this.unitService = unitService;
        this.carrierService = carrierService;
    }

    @GetMapping("/list")
    public String listCargos (Model model) {
        List<Cargo> cargos = cargoService.findAll();
        model.addAttribute("cargos", cargos);
        return "cargos/cargosList";
    }

    @GetMapping ("/retriveCargo")
    public String retriveCargo(Model model) {
        Cargo cargo = null;
        model.addAttribute("cargo", cargo);
        return "cargos/cargosList";
    }

    @GetMapping ("/showFormForAddCargo")
    public String showFormForUpdateCargo(Model model) {
        Cargo cargo = new Cargo();
        model.addAttribute("cargo", cargo);

        List<Customer> customers = customerService.findAll();
        model.addAttribute("customers", customers);

        List<Carrier> carriers = carrierService.findAll();
        model.addAttribute("carriers", carriers);

        List<Unit> units = unitService.findAll();
        model.addAttribute("units", units);

        return "cargos/cargoForm";
    }

    @GetMapping ("/showFormForUpdateCargo")
    public String showFormForUpdateCargo(@ModelAttribute ("cargoId") int id, Model model) {
        Cargo cargo = cargoService.findById(id);
        model.addAttribute("cargo", cargo);

        List<Customer> customers = customerService.findAll();
        model.addAttribute("customers", customers);


        List<Carrier> carriers = carrierService.findAll();
        model.addAttribute("carriers", carriers);

        List<Unit> units = unitService.findAll();
        model.addAttribute("units", units);

        return "cargos/cargoForm";
    }

    @PostMapping("/save")
    public String saveCargo (@Valid @ModelAttribute ("cargo") Cargo cargo, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {

            List<Customer> customers = customerService.findAll();
            model.addAttribute("customers", customers);


            List<Carrier> carriers = carrierService.findAll();
            model.addAttribute("carriers", carriers);

            List<Unit> units = unitService.findAll();
            model.addAttribute("units", units);

            return "cargos/cargoForm";
        } else {
            cargoService.save(cargo);
            return "redirect:list";
        }
    }

    @GetMapping("/delete")
    public String deleteCargo (@ModelAttribute ("cargoId") int id) {
        cargoService.deleteById(id);
        return "redirect:list";
    }
}
