package pl.GrzegorzKarolak.spedytor.controller.cargo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.GrzegorzKarolak.spedytor.entity.Cargo;
import pl.GrzegorzKarolak.spedytor.service.cargo.CargoService;

import java.util.List;

@Controller
@RequestMapping("/cargos")
public class CargoSearchController {

    private CargoService cargoService;

    @Autowired
    public CargoSearchController(CargoService cargoService) {
        this.cargoService = cargoService;
    }

    @GetMapping("/searchById")
    public String searchById(@ModelAttribute("searchId") String idString, Model model) {
        List<Cargo> cargos = null;
        if (!idString.isEmpty()) {
            int id = Integer.parseInt(idString);
            cargos = cargoService.findByIdAsc(id);
        } else {
            cargos = cargoService.findAll();
        }
        model.addAttribute("cargos", cargos);
        return "cargos/cargosList";
    }

    @GetMapping("/searchByCustomer")
    public String searchByCustomer(@ModelAttribute("searchCustomer") String customer, Model model) {
        List<Cargo> cargos = null;
        if (!customer.isEmpty()) {
            cargos = cargoService.findByCustomer(customer);
        } else {
            cargos = cargoService.findAll();
        }
        model.addAttribute("cargos", cargos);
        return "cargos/cargosList";
    }

    @GetMapping("/searchByOrderDate")
    public String searchByOrderDate(@ModelAttribute("searchOrderDate") String orderDate, Model model) {
        List<Cargo> cargos = null;
        if (!orderDate.isEmpty()) {
            cargos = cargoService.findByOrderDate(orderDate);
        } else {
            cargos = cargoService.findAll();
        }
        model.addAttribute("cargos", cargos);
        return "cargos/cargosList";
    }

    @GetMapping("/searchByPickupDate")
    public String searchByPickupDate(@ModelAttribute("searchPickupDate") String pickupDate, Model model) {
        List<Cargo> cargos = null;
        if (!pickupDate.isEmpty()) {
            cargos = cargoService.findByPickupDate(pickupDate);
        } else {
            cargos = cargoService.findAll();
        }
        model.addAttribute("cargos", cargos);
        return "cargos/cargosList";
    }

    @GetMapping("/searchByDeliveryDate")
    public String searchByDeliveryDate(@ModelAttribute("searchDeliveryDate") String deliveryDate, Model model) {
        List<Cargo> cargos = null;
        if (!deliveryDate.isEmpty()) {
            cargos = cargoService.findByDeliveryDate(deliveryDate);
        } else {
            cargos = cargoService.findAll();
        }
        model.addAttribute("cargos", cargos);
        return "cargos/cargosList";
    }

    @GetMapping("/searchByShippingFrom")
    public String searchByShippingFrom(@ModelAttribute("searchShippingFrom") String shippingFrom, Model model) {
        List<Cargo> cargos = null;
        if (!shippingFrom.isEmpty()) {
            cargos = cargoService.findByShippingFrom(shippingFrom);
        } else {
            cargos = cargoService.findAll();
        }
        model.addAttribute("cargos", cargos);
        return "cargos/cargosList";
    }

    @GetMapping("/searchByShippingTo")
    public String searchByShippingTo(@ModelAttribute("searchShippingTo") String shippingTo, Model model) {
        List<Cargo> cargos = null;
        if (!shippingTo.isEmpty()) {
            cargos = cargoService.findByShippingTo(shippingTo);
        } else {
            cargos = cargoService.findAll();
        }
        model.addAttribute("cargos", cargos);
        return "cargos/cargosList";
    }

    @GetMapping("/searchByCarrier")
    public String searchByCarrier(@ModelAttribute("searchCarrier") String carrier, Model model) {
        List<Cargo> cargos = null;
        if (!carrier.isEmpty()) {
            cargos = cargoService.findByCarrier(carrier);
        } else {
            cargos = cargoService.findAll();
        }
        model.addAttribute("cargos", cargos);
        return "cargos/cargosList";
    }

    @GetMapping("/searchByDescription")
    public String searchByDescription(@ModelAttribute("searchDescription") String description, Model model) {
        List<Cargo> cargos = null;
        if (!description.isEmpty()) {
            cargos = cargoService.findByDescription(description);
        } else {
            cargos = cargoService.findAll();
        }
        model.addAttribute("cargos", cargos);
        return "cargos/cargosList";
    }

    @GetMapping("/searchByCargoParameters")
    public String searchByCargoParameters(@ModelAttribute("searchCargoParameters") String cargoParameters, Model model) {
        List<Cargo> cargos = null;

        if (isNumeric(cargoParameters)) {
            Integer number = Integer.parseInt(cargoParameters);
            String string = null;
            cargos = cargoService.findByCargoParameters(number, string, number, number, string);
        } else if (cargoParameters.isEmpty()){
            cargos = cargoService.findAll();
        } else {
            Integer number = null;
            String string = cargoParameters;
            cargos = cargoService.findByCargoParameters(number, string, number, number, string);
        }
        model.addAttribute("cargos", cargos);
        return "cargos/cargosList";
        }

    public static boolean isNumeric(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch(NumberFormatException e){
            return false;
        }
    }

}
