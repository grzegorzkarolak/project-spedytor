package pl.GrzegorzKarolak.spedytor.controller.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.GrzegorzKarolak.spedytor.entity.Customer;
import pl.GrzegorzKarolak.spedytor.service.customer.CustomerService;

import java.util.List;

@Controller
@RequestMapping("/customers")
public class CustomerSearchController {

    private CustomerService customerService;

    @Autowired
    public CustomerSearchController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/searchById")
    public String searchById(@ModelAttribute("searchId") String idString, Model model) {
        List<Customer> customers = null;
        if (!idString.isEmpty()) {
            int id = Integer.parseInt(idString);
            customers = customerService.searchById(id);
        } else {
            customers = customerService.findAll();
        }
        model.addAttribute("customers", customers);
        return "customers/customersList";
    }

    @GetMapping("/searchByName")
    public String searchByName(@ModelAttribute("searchName") String name, Model model) {
        List<Customer> customers = null;
        if (!name.isEmpty()) {
            customers = customerService.searchByName(name);
        } else {
            customers = customerService.findAll();
        }
        model.addAttribute("customers", customers);
        return "customers/customersList";
    }

    @GetMapping("/searchByAddress")
    public String searchByAddress(@ModelAttribute("searchAddress") String address, Model model) {
        List<Customer> customers = null;
        if (!address.isEmpty()) {
            customers = customerService.searchByAddress(address);
        } else {
            customers = customerService.findAll();
        }
        model.addAttribute("customers", customers);
        return "customers/customersList";
    }

    @GetMapping("/searchByCountry")
    public String searchByCountry(@ModelAttribute("searchCountry") String country, Model model) {
        List<Customer> customers = null;
        if (!country.isEmpty()) {
            customers = customerService.searchByCountry(country);
        } else {
            customers = customerService.findAll();
        }
        model.addAttribute("customers", customers);
        return "customers/customersList";
    }

    @GetMapping("/searchByPerson")
    public String searchByPerson(@ModelAttribute("searchPerson") String person, Model model) {
        List<Customer> customers = null;
        if (!person.isEmpty()) {
            customers = customerService.searchByPerson(person);
        } else {
            customers = customerService.findAll();
        }
        model.addAttribute("customers", customers);
        return "customers/customersList";
    }

    @GetMapping("/searchByInvoiceDetails")
    public String searchByInvoiceDetails(@ModelAttribute("searchInvoiceDetails") String invoiceDetails, Model model) {
        List<Customer> customers = null;
        if (!invoiceDetails.isEmpty()) {
            customers = customerService.searchByInvoiceDetails(invoiceDetails);
        } else {
            customers = customerService.findAll();
        }
        model.addAttribute("customers", customers);
        return "customers/customersList";
    }
}


