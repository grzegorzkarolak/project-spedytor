package pl.GrzegorzKarolak.spedytor.controller.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.GrzegorzKarolak.spedytor.entity.Customer;
import pl.GrzegorzKarolak.spedytor.service.customer.CustomerService;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/customers")
public class CustomerController {

    private CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/list")
    public String listCustomers (Model model) {
        List<Customer> customers = customerService.findAll();
        model.addAttribute("customers", customers);
        return "customers/customersList";
    }

    @GetMapping ("/retriveCustomer")
    public String retriveCustomer(Model model) {
        Customer customer = null;
        model.addAttribute("customer", customer);
        return "customers/customersList";
    }

    @GetMapping ("/showFormForUpdateCustomer")
    public String showFormForUpdateCustomer(@ModelAttribute ("customerId") int id, Model model) {
        Customer customer = customerService.findById(id);
        model.addAttribute("customer", customer);
        return "customers/customerForm";
    }

    @GetMapping ("/showFormForAddCustomer")
    public String showFormForUpdateCustomer(Model model) {
        Customer customer = new Customer();
        model.addAttribute("customer", customer);
        return "customers/customerForm";
    }

    @RequestMapping("/save")
    public String saveCustomer (@Valid @ModelAttribute ("customer") Customer customer, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "customers/customerForm";
        } else {
            customerService.save(customer);
            return "redirect:list";
        }
    }

    @GetMapping("/delete")
    public String deleteCustomer (@ModelAttribute ("customerId") int id) {
        customerService.deleteById(id);
        return "redirect:list";
    }
}
