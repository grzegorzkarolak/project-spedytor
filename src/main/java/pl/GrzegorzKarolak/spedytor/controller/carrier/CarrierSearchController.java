package pl.GrzegorzKarolak.spedytor.controller.carrier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.GrzegorzKarolak.spedytor.entity.Carrier;
import pl.GrzegorzKarolak.spedytor.service.carrier.CarrierService;

import java.util.List;

@Controller
@RequestMapping("/carriers")
public class CarrierSearchController {

    private CarrierService carrierService;

    @Autowired
    public CarrierSearchController(CarrierService carrierService) {
        this.carrierService = carrierService;
    }

    @GetMapping("/searchById")
    public String searchById(@ModelAttribute("searchId") String idString, Model model) {
        List<Carrier> carriers = null;
        if (!idString.isEmpty()) {
            int id = Integer.parseInt(idString);
            carriers = carrierService.searchById(id);
        } else {
            carriers = carrierService.findAll();
        }
        model.addAttribute("carriers", carriers);
        return "carriers/carriersList";
    }

    @GetMapping("/searchByName")
    public String searchByName(@ModelAttribute("searchName") String name, Model model) {
        List<Carrier> carriers = null;
        if (!name.isEmpty()) {
            carriers = carrierService.searchByName(name);
        } else {
            carriers = carrierService.findAll();
        }
        model.addAttribute("carriers", carriers);
        return "carriers/carriersList";
    }

    @GetMapping("/searchByAddress")
    public String searchByAddress(@ModelAttribute("searchAddress") String address, Model model) {
        List<Carrier> carriers = null;
        if (!address.isEmpty()) {
            carriers = carrierService.searchByAddress(address);
        } else {
            carriers = carrierService.findAll();
        }
        model.addAttribute("carriers", carriers);
        return "carriers/carriersList";
    }

    @GetMapping("/searchByCountry")
    public String searchByCountry(@ModelAttribute("searchCountry") String country, Model model) {
        List<Carrier> carriers = null;
        if (!country.isEmpty()) {
            carriers = carrierService.searchByCountry(country);
        } else {
            carriers = carrierService.findAll();
        }
        model.addAttribute("carriers", carriers);
        return "carriers/carriersList";
    }

    @GetMapping("/searchByPerson")
    public String searchByPerson(@ModelAttribute("searchPerson") String person, Model model) {
        List<Carrier> carriers = null;
        if (!person.isEmpty()) {
            carriers = carrierService.searchByPerson(person);
        } else {
            carriers = carrierService.findAll();
        }
        model.addAttribute("carriers", carriers);
        return "carriers/carriersList";
    }
}


