package pl.GrzegorzKarolak.spedytor.controller.carrier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pl.GrzegorzKarolak.spedytor.entity.Carrier;
import pl.GrzegorzKarolak.spedytor.entity.Carrier;
import pl.GrzegorzKarolak.spedytor.entity.Driver;
import pl.GrzegorzKarolak.spedytor.entity.Truck;
import pl.GrzegorzKarolak.spedytor.service.carrier.CarrierService;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/carriers")
public class CarrierController {

    private CarrierService carrierService;

    @Autowired
    public CarrierController(CarrierService carrierService) {
        this.carrierService = carrierService;
    }

    @GetMapping("/list")
    public String listCarriers(Model model) {
        List<Carrier> carriers = carrierService.findAll();
        model.addAttribute("carriers", carriers);
        return "carriers/carriersList";
    }

    @GetMapping("/retriveCarrier")
    public String retriveCarrier(Model model) {
        Carrier carrier = null;
        model.addAttribute("carrier", carrier);
        return "carriers/carriersList";
    }

    @GetMapping("/showFormForUpdateCarrier")
    public String showFormForUpdateCarrier(@ModelAttribute("carrierId") Integer id, Model model) {
        Carrier carrier = carrierService.findById(id);
        model.addAttribute("carrier", carrier);
        return "carriers/carrierForm";
    }

    @GetMapping("/showFormForAddCarrier")
    public String showFormForUpdateCarrier(Model model) {
        Carrier carrier = new Carrier();
        model.addAttribute("carrier", carrier);
        return "carriers/carrierForm";
    }

    @GetMapping("/showTrucks")
    public String showTrucks(@ModelAttribute("carrierId") Integer id, Model model) {
        Carrier carrier = carrierService.findById(id);
        model.addAttribute("carrier", carrier);

        List<Truck> trucks = carrier.getTrucks();
        model.addAttribute("trucks", trucks);
        return "trucks/trucksList";
    }

    @GetMapping("/showDrivers")
    public String showDrivers(@ModelAttribute("carrierId") Integer id, Model model) {
        Carrier carrier = carrierService.findById(id);
        model.addAttribute("carrier", carrier);

        List<Driver> drivers = carrier.getDrivers();
        model.addAttribute("drivers", drivers);
        return "drivers/driversList";
    }

    @PostMapping("/save")
    public String saveCarrier(@Valid @ModelAttribute("carrier") Carrier carrier, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "carriers/carrierForm";
        } else {
            carrierService.save(carrier);
            return "redirect:list";
        }
    }

        @GetMapping("/delete")
        public String deleteCarrier (@RequestParam("carrierId") int id){
            carrierService.deleteById(id);
            return "redirect:list";
        }
    }
