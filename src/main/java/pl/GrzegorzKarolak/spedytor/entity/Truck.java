package pl.GrzegorzKarolak.spedytor.entity;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.List;

@Entity
@Table(name = "truck")
public class Truck {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @NotNull(message = "Truck Capacity is required")
    @Positive(message = "must be greater than 0")
    @Column(name = "capacity")
    private Integer capacity;

    @Column(name = "details")
    private String details;

    @NotBlank(message = "Truck Kind is required")
    @Column(name = "kind")
    private String kind;

    @NotBlank(message = "Truck Registration is required")
    @Column(name = "truck_reg")
    private String truckReg;

    @Column(name = "lorry_reg")
    private String lorryReg;

    @NotNull(message = "Carrier is required")
    @ManyToOne( cascade = { CascadeType.PERSIST,
                            CascadeType.MERGE,
                            CascadeType.DETACH,
                            CascadeType.REFRESH},
                fetch =     FetchType.LAZY)
    @JoinColumn(name="carrier_id")
    private Carrier carrier;

    @ManyToMany(fetch =     FetchType.LAZY,
                cascade = { CascadeType.PERSIST,
                            CascadeType.MERGE,
                            CascadeType.DETACH,
                            CascadeType.REFRESH } )
    @JoinTable( name = "driver_truck",
            joinColumns =        @JoinColumn(name = "truck_id"),
            inverseJoinColumns = @JoinColumn(name = "driver_id"))
    private List<Driver> drivers;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getTruckReg() {
        return truckReg;
    }

    public void setTruckReg(String truckReg) {
        this.truckReg = truckReg;
    }

    public String getLorryReg() {
        return lorryReg;
    }

    public void setLorryReg(String lorryReg) {
        this.lorryReg = lorryReg;
    }

    public Carrier getCarrier() {
        return carrier;
    }

    public void setCarrier(Carrier carrier) {
        this.carrier = carrier;
    }

    public List<Driver> getDrivers() {
        return drivers;
    }

    public void setDrivers(List<Driver> drivers) {
        this.drivers = drivers;
    }

    @Override
    public String toString() {
        return truckReg + lorryReg;
    }
}
