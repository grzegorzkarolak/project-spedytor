package pl.GrzegorzKarolak.spedytor.entity;

import pl.GrzegorzKarolak.spedytor.entity.documents.CarrierTransportOrder;
import pl.GrzegorzKarolak.spedytor.entity.helper.Address;
import pl.GrzegorzKarolak.spedytor.entity.helper.ContactDetails;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "carrier")
public class Carrier {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @NotBlank(message = "Name is required")
    @Column(name = "name")
    private String name;

    @Column(name = "default_payment_due_date")
    private Integer defaultPaymentDueDate;

    @NotNull
    @Valid
    @OneToOne(  cascade = CascadeType.ALL,
                fetch = FetchType.LAZY)
    @JoinColumn(name = "address_id")
    private Address address;

    @OneToOne(  cascade = CascadeType.ALL,
                fetch = FetchType.LAZY)
    @JoinColumn(name = "contact_details_id")
    private ContactDetails contactDetails;

    @OneToMany( mappedBy = "carrier",
                cascade =   CascadeType.ALL,
                fetch =     FetchType.LAZY)
    private List<CarrierTransportOrder> carrierTransportOrders;

    @OneToMany( mappedBy = "carrier",
                cascade =   CascadeType.ALL,
                fetch =     FetchType.LAZY)
    private List<Truck> trucks;

    @OneToMany( mappedBy = "carrier",
                cascade =  CascadeType.ALL,
                fetch =     FetchType.LAZY)

    private List<Driver> drivers;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDefaultPaymentDueDate() {
        return defaultPaymentDueDate;
    }

    public void setDefaultPaymentDueDate(Integer defaultPaymentDueDate) {
        this.defaultPaymentDueDate = defaultPaymentDueDate;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public ContactDetails getContactDetails() {
        return contactDetails;
    }

    public void setContactDetails(ContactDetails contactDetails) {
        this.contactDetails = contactDetails;
    }

    public List<CarrierTransportOrder> getCarrierTransportOrders() {
        return carrierTransportOrders;
    }

    public void setCarrierTransportOrders(List<CarrierTransportOrder> carrierTransportOrders) {
        this.carrierTransportOrders = carrierTransportOrders;
    }

    public List<Truck> getTrucks() {
        return trucks;
    }

    public void setTrucks(List<Truck> trucks) {
        this.trucks = trucks;
    }

    public List<Driver> getDrivers() {
        return drivers;
    }

    public void setDrivers(List<Driver> drivers) {
        this.drivers = drivers;
    }

    @Override
    public String toString() {
        return name;
    }
}
