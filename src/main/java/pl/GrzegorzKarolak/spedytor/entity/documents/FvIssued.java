package pl.GrzegorzKarolak.spedytor.entity.documents;

import javax.persistence.*;

@Entity
@Table(name = "fv_issued")
public class FvIssued {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "is_paid",
            nullable = false,
            columnDefinition = "BOOLEAN")
    private boolean isPaid;

    @OneToOne(  mappedBy =  "fvIssued",
                cascade = { CascadeType.DETACH,
                            CascadeType.MERGE,
                            CascadeType.PERSIST,
                            CascadeType.REFRESH},
                fetch =     FetchType.LAZY)
    private CustomerTransportOrder customerTransportOrder;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isPaid() {
        return isPaid;
    }

    public void setPaid(boolean paid) {
        isPaid = paid;
    }

    public CustomerTransportOrder getCustomerTransportOrder() {
        return customerTransportOrder;
    }

    public void setCustomerTransportOrder(CustomerTransportOrder customerTransportOrder) {
        this.customerTransportOrder = customerTransportOrder;
    }

    @Override
    public String toString() {
        return id.toString();
    }
}
