package pl.GrzegorzKarolak.spedytor.entity.documents;


import pl.GrzegorzKarolak.spedytor.entity.Cargo;

import javax.persistence.*;

@Entity
@Table(name = "cmr")
public class Cmr {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @OneToOne(  mappedBy =  "cmr",
                cascade = { CascadeType.DETACH,
                            CascadeType.MERGE,
                            CascadeType.PERSIST,
                            CascadeType.REFRESH},
                fetch =     FetchType.LAZY)
    private Cargo cargo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    @Override
    public String toString() {
        return id.toString();
    }
}
