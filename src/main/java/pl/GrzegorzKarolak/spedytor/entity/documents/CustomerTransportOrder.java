package pl.GrzegorzKarolak.spedytor.entity.documents;

import pl.GrzegorzKarolak.spedytor.entity.Cargo;
import pl.GrzegorzKarolak.spedytor.entity.Customer;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "customer_transport_order")
public class CustomerTransportOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "cost")
    private Double cost;

    @Column(name = "payment_due_date")
    private Integer paymentDueDate;

    @NotNull(message = "You must choose customer who placed the order")
    @ManyToOne( cascade = { CascadeType.PERSIST,
                            CascadeType.MERGE,
                            CascadeType.DETACH,
                            CascadeType.REFRESH},
                fetch =     FetchType.LAZY)
    @JoinColumn(name="customer_id")
    private Customer customer;

    @OneToMany( mappedBy = "customerTransportOrder",
                cascade = { CascadeType.DETACH,
                            CascadeType.MERGE,
                            CascadeType.PERSIST,
                            CascadeType.REFRESH},
                fetch =     FetchType.LAZY)
    private List<Cargo> cargos;

    @OneToOne(  cascade = { CascadeType.PERSIST,
                            CascadeType.MERGE,
                            CascadeType.DETACH,
                            CascadeType.REFRESH},
                fetch =     FetchType.LAZY)
    @JoinColumn(name = "fv_issued_id")
    private FvIssued fvIssued;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Integer getPaymentDueDate() {
        return paymentDueDate;
    }

    public void setPaymentDueDate(Integer paymentDueDate) {
        this.paymentDueDate = paymentDueDate;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<Cargo> getCargos() {
        return cargos;
    }

    public void setCargos(List<Cargo> cargos) {
        this.cargos = cargos;
    }

    public FvIssued getFvIssued() {
        return fvIssued;
    }

    public void setFvIssued(FvIssued fvIssued) {
        this.fvIssued = fvIssued;
    }

    @Override
    public String toString() {
        return id.toString();
    }
}
