package pl.GrzegorzKarolak.spedytor.entity.documents;

import javax.persistence.*;

@Entity
@Table(name = "fv_recived")
public class FvRecived {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "is_paid",
            nullable = false,
            columnDefinition = "BOOLEAN")
    private boolean isPaid;

    @OneToOne(  mappedBy =  "fvRecived",
                cascade = { CascadeType.DETACH,
                            CascadeType.MERGE,
                            CascadeType.PERSIST,
                            CascadeType.REFRESH},
                fetch =     FetchType.LAZY)
    private CarrierTransportOrder carrierTransportOrder;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isPaid() {
        return isPaid;
    }

    public void setPaid(boolean paid) {
        isPaid = paid;
    }

    public CarrierTransportOrder getCarrierTransportOrder() {
        return carrierTransportOrder;
    }

    public void setCarrierTransportOrder(CarrierTransportOrder carrierTransportOrder) {
        this.carrierTransportOrder = carrierTransportOrder;
    }

    @Override
    public String toString() {
        return id.toString();
    }
}
