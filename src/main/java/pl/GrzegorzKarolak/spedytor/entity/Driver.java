package pl.GrzegorzKarolak.spedytor.entity;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.List;

@Entity
@Table(name = "driver")
public class Driver {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @NotBlank(message = "First Name is required")
    @Column(name = "first_name")
    private String firstName;

    @NotBlank(message = "Last Name is required")
    @Column(name = "last_name")
    private String lastName;

    @Pattern(regexp = "^[0-9]*$*", message = "only numbers")
    @Column(name = "phone_no")
    private String phoneNo;

    @NotNull(message = "ADR is required")
    @Column(name = "have_adr")
    private Boolean haveADR;

    @Column(name = "details")
    private String details;

    @NotNull(message = "Carrier is required")
    @ManyToOne( cascade = { CascadeType.PERSIST,
                            CascadeType.MERGE,
                            CascadeType.DETACH,
                            CascadeType.REFRESH},
                fetch =     FetchType.LAZY)
    @JoinColumn(name="carrier_id")
    private Carrier carrier;

    @ManyToMany(fetch =     FetchType.LAZY,
                cascade = { CascadeType.PERSIST,
                            CascadeType.MERGE,
                            CascadeType.DETACH,
                            CascadeType.REFRESH } )
    @JoinTable( name = "driver_truck",
            joinColumns =        @JoinColumn(name = "driver_id"),
            inverseJoinColumns = @JoinColumn(name = "truck_id"))
    private List<Truck> trucks;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public Boolean getHaveADR() {
        return haveADR;
    }

    public void setHaveADR(Boolean haveADR) {
        this.haveADR = haveADR;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Carrier getCarrier() {
        return carrier;
    }

    public void setCarrier(Carrier carrier) {
        this.carrier = carrier;
    }

    public List<Truck> getTrucks() {
        return trucks;
    }

    public void setTrucks(List<Truck> trucks) {
        this.trucks = trucks;
    }

    @Override
    public String toString() {
        return firstName + lastName;
    }
}
