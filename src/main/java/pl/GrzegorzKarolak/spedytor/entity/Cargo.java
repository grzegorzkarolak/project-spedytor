package pl.GrzegorzKarolak.spedytor.entity;

import org.springframework.format.annotation.DateTimeFormat;
import pl.GrzegorzKarolak.spedytor.entity.documents.CarrierTransportOrder;
import pl.GrzegorzKarolak.spedytor.entity.documents.Cmr;
import pl.GrzegorzKarolak.spedytor.entity.documents.CustomerTransportOrder;
import pl.GrzegorzKarolak.spedytor.entity.helper.Address;
import pl.GrzegorzKarolak.spedytor.entity.helper.CargoParameters;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "cargo")
public class Cargo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "description")
    private String description;

    @Column(name = "cargo_content_type")
    private String cargoContentType;

    @NotNull(message = "Order Date required")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Temporal(TemporalType.DATE)
    @Column(name = "order_date")
    private Date orderDate;

    @NotNull(message = "Pick Up Date required")
    @Column(name = "pickup_date")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @Temporal(TemporalType.DATE)
    private Date pickupDate;

    @NotNull(message = "Delivery Date required")
    @Column(name = "delivery_date")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @Temporal(TemporalType.DATE)
    private Date deliveryDate;

    @Column(name = "total_weight")
    private Integer totalWeight;

    @Positive(message = "Value must be in currency format, ex. 12.34")
    @Column(name = "cargo_value")
    private Double cargoValue;

    @Valid
    @NotNull
    @ManyToOne( cascade =   CascadeType.ALL,
                fetch =     FetchType.LAZY)
    @JoinColumn(name="shipping_from_id")
    private Address shippingFrom;

    @Valid
    @NotNull
    @ManyToOne( cascade =   CascadeType.ALL,
                fetch =     FetchType.LAZY)
    @JoinColumn(name="shipping_to_id")
    private Address shippingTo;

    @Valid
    @NotNull
    @ManyToOne( cascade = { CascadeType.PERSIST,
                            CascadeType.MERGE,
                            CascadeType.DETACH,
                            CascadeType.REFRESH},
                fetch =     FetchType.LAZY)
    @JoinColumn(name="customer_transport_order_id")
    private CustomerTransportOrder customerTransportOrder;

    @Valid
    @NotNull
    @ManyToOne( cascade = { CascadeType.PERSIST,
                            CascadeType.MERGE,
                            CascadeType.DETACH,
                            CascadeType.REFRESH},
                fetch =     FetchType.LAZY)
    @JoinColumn(name="carrier_transport_order_id")
    private CarrierTransportOrder carrierTransportOrder;

    @OneToOne(   cascade = CascadeType.ALL,
                 fetch =   FetchType.LAZY)
    @JoinColumn(name = "cmr_id")
    private Cmr cmr;

    @NotNull
    @Valid
    @ManyToMany(fetch =     FetchType.LAZY,
                cascade = { CascadeType.PERSIST,
                            CascadeType.MERGE,
                            CascadeType.DETACH,
                            CascadeType.REFRESH } )
    @JoinTable( name = "cargos_parameters",
                joinColumns =        @JoinColumn(name = "cargo_id"),
                inverseJoinColumns = @JoinColumn(name = "parameters_of_cargo_id"))
    private List<CargoParameters> cargoParameters;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCargoContentType() {
        return cargoContentType;
    }

    public void setCargoContentType(String cargoContentType) {
        this.cargoContentType = cargoContentType;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Date getPickupDate() {
        return pickupDate;
    }

    public void setPickupDate(Date pickupDate) {
        this.pickupDate = pickupDate;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Integer getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(Integer totalWeight) {
        this.totalWeight = totalWeight;
    }

    public Double getCargoValue() {
        return cargoValue;
    }

    public void setCargoValue(Double cargoValue) {
        this.cargoValue = cargoValue;
    }

    public Address getShippingFrom() {
        return shippingFrom;
    }

    public void setShippingFrom(Address shippingFrom) {
        this.shippingFrom = shippingFrom;
    }

    public Address getShippingTo() {
        return shippingTo;
    }

    public void setShippingTo(Address shippingTo) {
        this.shippingTo = shippingTo;
    }

    public CustomerTransportOrder getCustomerTransportOrder() {
        return customerTransportOrder;
    }

    public void setCustomerTransportOrder(CustomerTransportOrder customerTransportOrder) {
        this.customerTransportOrder = customerTransportOrder;
    }

    public CarrierTransportOrder getCarrierTransportOrder() {
        return carrierTransportOrder;
    }

    public void setCarrierTransportOrder(CarrierTransportOrder carrierTransportOrder) {
        this.carrierTransportOrder = carrierTransportOrder;
    }

    public Cmr getCmr() {
        return cmr;
    }

    public void setCmr(Cmr cmr) {
        this.cmr = cmr;
    }

    public List<CargoParameters> getCargoParameters() {
        return cargoParameters;
    }

    public void setCargoParameters(List<CargoParameters> cargoParameters) {
        this.cargoParameters = cargoParameters;
    }

    @Override
    public String toString() {
        return id.toString();
    }
}
