package pl.GrzegorzKarolak.spedytor.entity;

import pl.GrzegorzKarolak.spedytor.entity.documents.CustomerTransportOrder;
import pl.GrzegorzKarolak.spedytor.entity.helper.Address;
import pl.GrzegorzKarolak.spedytor.entity.helper.ContactDetails;
import pl.GrzegorzKarolak.spedytor.entity.helper.InvoiceDetails;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "customer")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @NotBlank (message = "Name of company is required")
    @Column(name = "name")
    private String name;

    @NotNull
    @Valid
    @OneToOne(  cascade = CascadeType.ALL,
                fetch = FetchType.EAGER)
    @JoinColumn(name = "address_id")
    private Address address;

    @OneToOne(  cascade = CascadeType.ALL,
                fetch = FetchType.EAGER)
    @JoinColumn(name = "contact_details_id")
    private ContactDetails contactDetails;

    @NotNull
    @Valid
    @OneToOne(  cascade = CascadeType.ALL,
                fetch = FetchType.EAGER)
    @JoinColumn(name = "invoice_details_id")
    private InvoiceDetails invoiceDetails;

    @OneToMany( mappedBy = "customer",
                cascade =  CascadeType.ALL,
                fetch =     FetchType.EAGER)
    private List<CustomerTransportOrder> customerTransportOrders;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public ContactDetails getContactDetails() {
        return contactDetails;
    }

    public void setContactDetails(ContactDetails contactDetails) {
        this.contactDetails = contactDetails;
    }

    public InvoiceDetails getInvoiceDetails() {
        return invoiceDetails;
    }

    public void setInvoiceDetails(InvoiceDetails invoiceDetails) {
        this.invoiceDetails = invoiceDetails;
    }

    public List<CustomerTransportOrder> getCustomerTransportOrders() {
        return customerTransportOrders;
    }

    public void setCustomerTransportOrders(List<CustomerTransportOrder> customerTransportOrders) {
        this.customerTransportOrders = customerTransportOrders;
    }

    @Override
    public String toString() {
        return name;
    }
}