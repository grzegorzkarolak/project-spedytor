package pl.GrzegorzKarolak.spedytor.entity.helper;

import pl.GrzegorzKarolak.spedytor.entity.Cargo;
import pl.GrzegorzKarolak.spedytor.entity.Carrier;
import pl.GrzegorzKarolak.spedytor.entity.Customer;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.List;

@Entity
@Table(name="address")
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;

    @NotBlank(message = "Name of Address is required")
    @Column(name="name")
    private String name;

    @NotBlank(message = "First Address Line is required")
    @Column(name="first_address_line")
    private String firstAddressLine;

    @Column(name="second_address_line")
    private String secondAddressLine;

    @NotBlank(message = "City is required")
    @Column(name="city")
    private String city;

    @NotBlank(message = "Zip Code is required")
    @Column(name="zip_code")
    private String zipCode;

    @NotBlank(message = "Country is required")
    @Column(name="country")
    private String country;

    @Pattern(regexp = "^[0-9]*$*", message = "only numbers")
    @Column(name="phone_number")
    private String phoneNumber;

    @OneToOne(  mappedBy =  "address",
                cascade = { CascadeType.DETACH,
                            CascadeType.MERGE,
                            CascadeType.PERSIST,
                            CascadeType.REFRESH},
                fetch =     FetchType.LAZY)
    private Customer customer;

    @OneToOne(  mappedBy =  "address",
                cascade = { CascadeType.DETACH,
                            CascadeType.MERGE,
                            CascadeType.PERSIST,
                            CascadeType.REFRESH},
                fetch =     FetchType.LAZY)
    private InvoiceDetails invoiceDetails;

    @OneToOne(  mappedBy =  "address",
                cascade = { CascadeType.DETACH,
                            CascadeType.MERGE,
                            CascadeType.PERSIST,
                            CascadeType.REFRESH},
                fetch =     FetchType.LAZY)
    private Carrier carrier;

    @OneToMany( mappedBy = "shippingFrom",
                cascade = { CascadeType.DETACH,
                            CascadeType.MERGE,
                            CascadeType.PERSIST,
                            CascadeType.REFRESH},
                fetch =     FetchType.LAZY)
    private List<Cargo> cargosFrom;

    @OneToMany( mappedBy = "shippingTo",
                cascade = { CascadeType.DETACH,
                            CascadeType.MERGE,
                            CascadeType.PERSIST,
                            CascadeType.REFRESH},
                fetch =     FetchType.LAZY)
    private List<Cargo> cargosTo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstAddressLine() {
        return firstAddressLine;
    }

    public void setFirstAddressLine(String firstAddressLine) {
        this.firstAddressLine = firstAddressLine;
    }

    public String getSecondAddressLine() {
        return secondAddressLine;
    }

    public void setSecondAddressLine(String secondAddressLine) {
        this.secondAddressLine = secondAddressLine;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public InvoiceDetails getInvoiceDetails() {
        return invoiceDetails;
    }

    public void setInvoiceDetails(InvoiceDetails invoiceDetails) {
        this.invoiceDetails = invoiceDetails;
    }

    public Carrier getCarrier() {
        return carrier;
    }

    public void setCarrier(Carrier carrier) {
        this.carrier = carrier;
    }

    public List<Cargo> getCargosFrom() {
        return cargosFrom;
    }

    public void setCargosFrom(List<Cargo> cargosFrom) {
        this.cargosFrom = cargosFrom;
    }

    public List<Cargo> getCargosTo() {
        return cargosTo;
    }

    public void setCargosTo(List<Cargo> cargosTo) {
        this.cargosTo = cargosTo;
    }

    @Override
    public String toString() {
        return name;
    }
}
