package pl.GrzegorzKarolak.spedytor.entity.helper;

import pl.GrzegorzKarolak.spedytor.entity.Cargo;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.List;

@Entity
@Table(name = "parameters_of_cargo")  //TODO rename table in database to cargo_parameters
public class CargoParameters {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @NotNull(message = "Quantity is required")
    @Positive(message = "must be greater than 0")
    @Column(name = "quantity")
    private Integer quantity;

    @NotNull(message = "Weight is required")
    @Positive(message = "must be greater than 0")
    @Column(name = "weight")
    private Integer weight;

    @NotNull(message = "Weight is required")
    @Positive(message = "must be greater than 0")
    @Column(name = "height")
    private Integer height;

    @Column(name = "volume")
    private Double volume;

    @ManyToMany(fetch =     FetchType.LAZY,
                cascade = { CascadeType.PERSIST,
                            CascadeType.MERGE,
                            CascadeType.DETACH,
                            CascadeType.REFRESH} )
    @JoinTable( name = "cargos_parameters",
                joinColumns =        @JoinColumn(name = "parameters_of_cargo_id"),
                inverseJoinColumns = @JoinColumn(name = "cargo_id") )
    private List<Cargo> cargos;

    @NotNull(message = "Kind od Unit is required")
    @ManyToOne( cascade = { CascadeType.PERSIST,
                            CascadeType.MERGE,
                            CascadeType.DETACH,
                            CascadeType.REFRESH},
                fetch =     FetchType.LAZY)
    @JoinColumn(name="unit_id")
    private Unit unit;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }

    public List<Cargo> getCargos() {
        return cargos;
    }

    public void setCargos(List<Cargo> cargos) {
        this.cargos = cargos;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    @Override
    public String toString() {
        return id.toString();
    }
}
