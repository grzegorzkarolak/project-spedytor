package pl.GrzegorzKarolak.spedytor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.GrzegorzKarolak.spedytor.entity.Customer;

import java.util.List;


public interface CustomerRepository extends JpaRepository<Customer, Integer> {

    List<Customer> findAllByOrderByNameAsc();

    @Query("Select c from Customer c WHERE c.name like %:name%")
    List<Customer> findByNameIgnoreCase(@Param("name") String name);

    @Query("Select c from Customer c WHERE c.id like :id")    // TODO
    List<Customer> findByIdAsc(@Param("id") Integer id);

    @Query("Select c from Customer c WHERE " +
            "c.address.name like %:address% or " +
            "c.address.firstAddressLine like %:address% or " +
            "c.address.secondAddressLine like %:address% or " +
            "c.address.zipCode like %:address% or " +
            "c.address.city like %:address% or " +
            "c.address.phoneNumber like %:address%")
    List<Customer> findByAddress (@Param("address") String address);

    @Query("Select c from Customer c WHERE c.address.country like %:country%")
    List<Customer>findByCountry(@Param("country") String country);

    @Query("Select c from Customer c WHERE " +
            "c.contactDetails.name like %:person% or " +
            "c.contactDetails.eMail like %:person% or " +
            "c.contactDetails.phoneNumber like %:person%")
    List<Customer> findByPerson(@Param("person") String person);

    @Query("Select c from Customer c WHERE " +
            "c.invoiceDetails.nip like %:invoiceDetails% or " +
            "c.invoiceDetails.address.name like %:invoiceDetails% or " +
            "c.invoiceDetails.address.firstAddressLine like %:invoiceDetails% or " +
            "c.invoiceDetails.address.secondAddressLine like %:invoiceDetails% or " +
            "c.invoiceDetails.address.zipCode like %:invoiceDetails% or " +
            "c.invoiceDetails.address.city like %:invoiceDetails%")
    List<Customer> findByInvoiceDetails(@Param("invoiceDetails") String invoiceDetails);
}
