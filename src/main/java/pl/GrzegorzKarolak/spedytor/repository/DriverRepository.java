package pl.GrzegorzKarolak.spedytor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.GrzegorzKarolak.spedytor.entity.Driver;

import java.util.List;


public interface DriverRepository extends JpaRepository<Driver, Integer> {

    List<Driver> findAllByOrderByLastNameAsc();
    
    @Query("Select t from Driver t WHERE t.id like :id")
    List<Driver> findByIdAsc(@Param("id") Integer id);

    @Query("Select t from Driver t WHERE t.firstName like %:firstName%")
    List<Driver> findByFirstName(@Param("firstName") String firstName);

    @Query("Select t from Driver t WHERE t.lastName like %:lastName%")
    List<Driver> findByLastName(@Param("lastName") String lastName);

    @Query("Select t from Driver t WHERE t.phoneNo like %:phoneNo%")
    List<Driver> findByPhoneNo(@Param("phoneNo") String phoneNo);

    @Query("Select t from Driver t WHERE t.haveADR like %:haveADR%")
    List<Driver> findByHaveADR(@Param("haveADR") Boolean haveADR);

    @Query("Select t from Driver t WHERE t.details like %:details%")
    List<Driver> findByDetails(@Param("details") String details);

    @Query("Select t from Driver t WHERE t.carrier.name like %:carrier%")
    List<Driver> findByCarrierName(@Param("carrier") String carrier);
}
