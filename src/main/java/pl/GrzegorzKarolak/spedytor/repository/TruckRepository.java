package pl.GrzegorzKarolak.spedytor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.GrzegorzKarolak.spedytor.entity.Driver;
import pl.GrzegorzKarolak.spedytor.entity.Truck;

import java.util.List;


public interface TruckRepository extends JpaRepository<Truck, Integer> {

    List<Truck> findAllByOrderByTruckRegAsc();
    
    @Query("Select t from Truck t WHERE t.id like :id")
    List<Truck> findByIdAsc(@Param("id") Integer id);

    @Query("Select t from Truck t WHERE t.truckReg like %:truckReg%")
    List<Truck> findByTruckReg(@Param("truckReg") String truckReg);

    @Query("Select t from Truck t WHERE t.lorryReg like %:lorryReg%")
    List<Truck> findByLorryReg(@Param("lorryReg") String lorryReg);

    @Query("Select t from Truck t WHERE t.kind like %:kind%")
    List<Truck> findByKind(@Param("kind") String kind);

    @Query("Select t from Truck t WHERE t.capacity like :capacity")
    List<Truck> findByCapacity(@Param("capacity") Integer capacity);

    @Query("Select t from Truck t WHERE t.details like %:details%")
    List<Truck> findByDetails(@Param("details") String details);

    @Query("Select t from Truck t WHERE t.carrier.name like %:carrier%")
    List<Truck> findByCarrierName(@Param("carrier") String carrier);

}
