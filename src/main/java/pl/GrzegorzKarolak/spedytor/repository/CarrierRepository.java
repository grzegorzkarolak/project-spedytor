package pl.GrzegorzKarolak.spedytor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.GrzegorzKarolak.spedytor.entity.Carrier;

import java.util.List;


public interface CarrierRepository extends JpaRepository<Carrier, Integer> {

    List<Carrier> findAllByOrderByNameAsc();

    @Query("Select c from Carrier c WHERE c.name like %:name%")
    List<Carrier> findByNameIgnoreCase(@Param("name") String name);

    @Query("Select c from Carrier c WHERE c.id like :id")
    List<Carrier> findByIdAsc(@Param("id") Integer id);

    @Query("Select c from Carrier c WHERE " +
            "c.address.name like %:address% or " +
            "c.address.firstAddressLine like %:address% or " +
            "c.address.secondAddressLine like %:address% or " +
            "c.address.zipCode like %:address% or " +
            "c.address.city like %:address% or " +
            "c.address.phoneNumber like %:address%")
    List<Carrier> findByAddress(@Param("address") String address);

    @Query("Select c from Carrier c WHERE c.address.country like %:country%")
    List<Carrier>findByCountry(@Param("country") String country);

    @Query("Select c from Carrier c WHERE " +
            "c.contactDetails.name like %:person% or " +
            "c.contactDetails.eMail like %:person% or " +
            "c.contactDetails.phoneNumber like %:person%")
    List<Carrier> findByPerson(@Param("person") String person);

}
