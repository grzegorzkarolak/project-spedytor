package pl.GrzegorzKarolak.spedytor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.GrzegorzKarolak.spedytor.entity.helper.Unit;

import java.util.List;


public interface UnitRepository extends JpaRepository<Unit, Integer> {

    List<Unit> findAllByOrderByNameAsc();

    @Query("Select u from Unit u WHERE u.name like %:name%")
    List<Unit> findByNameIgnoreCase(@Param("name") String name);

    @Query("Select u from Unit u WHERE u.id like :id")
    List<Unit> findByIdAsc(@Param("id") int id);

    @Query("Select u from Unit u WHERE u.width like :width")
    List<Unit> findByWidth(@Param("width") int width);

    @Query("Select u from Unit u WHERE u.length like :length")
    List<Unit> findByLength(@Param("length") int length);

    @Query("Select u from Unit u WHERE u.description like %:description%")
    List<Unit> findByDescription(@Param("description") String description);

}
