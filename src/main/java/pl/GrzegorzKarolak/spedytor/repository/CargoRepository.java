package pl.GrzegorzKarolak.spedytor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.GrzegorzKarolak.spedytor.entity.Cargo;

import java.util.Date;
import java.util.List;


public interface CargoRepository extends JpaRepository<Cargo, Integer> {

    @Query("Select c from Cargo c WHERE c.id like :id")
    List<Cargo> findByIdAsc(@Param("id") int id);

    @Query("Select c from Cargo c WHERE c.customerTransportOrder.customer.name like %:customer%")
    List<Cargo> findByCustomer(@Param("customer") String customer);

    @Query("Select c from Cargo c WHERE c.orderDate like %:orderDate%")
    List<Cargo> findByOrderDate(@Param("orderDate") String orderDate);

    @Query("Select c from Cargo c WHERE c.pickupDate like %:pickupDate%")
    List<Cargo> findByPickupDate(@Param("pickupDate") String pickupDate);

    @Query("Select c from Cargo c WHERE c.deliveryDate like %:deliveryDate%")
    List<Cargo> findByDeliveryDate(@Param("deliveryDate") String deliveryDate);

    @Query("Select c from Cargo c WHERE " +
            "c.shippingFrom.name like %:shippingFrom% or " +
            "c.shippingFrom.firstAddressLine like %:shippingFrom% or " +
            "c.shippingFrom.secondAddressLine like %:shippingFrom% or " +
            "c.shippingFrom.zipCode like %:shippingFrom% or " +
            "c.shippingFrom.city like %:shippingFrom% or " +
            "c.shippingFrom.phoneNumber like %:shippingFrom%")
    List<Cargo> findByShippingFrom(@Param("shippingFrom") String shippingFrom);

    @Query("Select c from Cargo c WHERE " +
            "c.shippingTo.name like %:shippingTo% or " +
            "c.shippingTo.firstAddressLine like %:shippingTo% or " +
            "c.shippingTo.secondAddressLine like %:shippingTo% or " +
            "c.shippingTo.zipCode like %:shippingTo% or " +
            "c.shippingTo.city like %:shippingTo% or " +
            "c.shippingTo.phoneNumber like %:shippingTo%")
    List<Cargo> findByShippingTo(@Param("shippingTo") String shippingTo);

    @Query("Select c from Cargo c WHERE c.carrierTransportOrder.carrier.name like %:carrier%")
    List<Cargo> findByCarrier(@Param("carrier") String carrier);

    List<Cargo> findByCargoParametersQuantityOrCargoParametersUnitNameOrCargoParametersWeightOrCargoParametersHeightOrCargoContentType
                (Integer quantity, String name, Integer weight, Integer height, String cargoContentType);

    @Query("Select c from Cargo c WHERE c.description like %:description%")
    List<Cargo> findByDescription(@Param("description") String description);

}

