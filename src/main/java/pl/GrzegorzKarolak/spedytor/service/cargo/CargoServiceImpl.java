package pl.GrzegorzKarolak.spedytor.service.cargo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.GrzegorzKarolak.spedytor.entity.Cargo;
import pl.GrzegorzKarolak.spedytor.entity.Customer;
import pl.GrzegorzKarolak.spedytor.repository.CargoRepository;

import java.util.List;
import java.util.Optional;

@Service
public class CargoServiceImpl implements CargoService {

    private CargoRepository cargoRepository;

    @Autowired
    public CargoServiceImpl(CargoRepository cargoRepository) {
        this.cargoRepository = cargoRepository;
    }

    @Override
    public List<Cargo> findAll() {
        return cargoRepository.findAll();
    }

    @Override
    public Cargo findById(int id) {
        Optional<Cargo> result = cargoRepository.findById(id);
        Cargo cargo = null;
        if(result.isPresent()) {
            cargo = result.get();
        } else {
            throw new RuntimeException("Did not find id cargo: " + id);
        }
        return cargo;
    }

    @Override
    public List<Cargo> findByIdAsc(int id) {
        return cargoRepository.findByIdAsc(id);
    }

    @Override
    public List<Cargo> findByCustomer(String customer) {
        return cargoRepository.findByCustomer(customer);
    }

    @Override
    public List<Cargo> findByOrderDate(String orderDate) {
        return cargoRepository.findByOrderDate(orderDate);
    }

    @Override
    public List<Cargo> findByPickupDate(String pickupDate) {
        return cargoRepository.findByPickupDate(pickupDate);
    }

    @Override
    public List<Cargo> findByDeliveryDate(String deliveryDate) {
        return cargoRepository.findByDeliveryDate(deliveryDate);
    }

    @Override
    public List<Cargo> findByShippingFrom(String shippingFrom) {
        return cargoRepository.findByShippingFrom(shippingFrom);
    }

    @Override
    public List<Cargo> findByShippingTo(String shippingTo) {
        return cargoRepository.findByShippingTo(shippingTo);
    }

    @Override
    public List<Cargo> findByCarrier(String carrier) {
        return cargoRepository.findByCarrier(carrier);
    }

    @Override
    public List<Cargo> findByDescription(String description) {
        return cargoRepository.findByDescription(description);
    }

    @Override
    public List<Cargo> findByCargoParameters(Integer quantity, String name, Integer weight, Integer height, String cargoContentType) {
        return cargoRepository.findByCargoParametersQuantityOrCargoParametersUnitNameOrCargoParametersWeightOrCargoParametersHeightOrCargoContentType
                                            (quantity, name, weight, height, cargoContentType);
    }

    @Override
    public void save(Cargo cargo) {
        cargoRepository.save(cargo);
    }

    @Override
    public void deleteById(int id) {
        cargoRepository.deleteById(id);
    }



}
