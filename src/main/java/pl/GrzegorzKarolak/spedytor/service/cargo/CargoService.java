package pl.GrzegorzKarolak.spedytor.service.cargo;

import pl.GrzegorzKarolak.spedytor.entity.Cargo;

import java.util.List;

public interface CargoService {

    List<Cargo> findAll();

    Cargo findById(int theId);

    List<Cargo> findByIdAsc(int id);

    List<Cargo> findByCustomer(String customer);

    List<Cargo> findByOrderDate(String orderDate);

    List<Cargo> findByPickupDate(String pickupDate);

    List<Cargo> findByDeliveryDate(String deliveryDate);

    List<Cargo> findByShippingFrom(String shippingFrom);

    List<Cargo> findByShippingTo(String shippingTo);

    List<Cargo> findByCarrier(String carrier);

    List<Cargo> findByDescription(String description);

    List<Cargo> findByCargoParameters(Integer quantity, String name, Integer weight, Integer height, String cargoContentType);

    void save(Cargo cargo);

    void deleteById(int id);



}
