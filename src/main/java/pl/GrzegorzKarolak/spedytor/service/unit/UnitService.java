package pl.GrzegorzKarolak.spedytor.service.unit;

import pl.GrzegorzKarolak.spedytor.entity.helper.Unit;

import java.util.List;

public interface UnitService {

    List<Unit> findAll();

    Unit findById(int theId);

    List<Unit> searchByName(String name);

    List<Unit> searchById(int id);

    List<Unit> searchByWidth (int width);

    List<Unit> searchByLength (int length);

    List<Unit> searchDscription(String description);

    void save(Unit unit);

    void deleteById(int id);
}
