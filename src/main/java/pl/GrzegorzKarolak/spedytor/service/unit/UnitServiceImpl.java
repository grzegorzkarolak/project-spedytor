package pl.GrzegorzKarolak.spedytor.service.unit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.GrzegorzKarolak.spedytor.entity.helper.Unit;
import pl.GrzegorzKarolak.spedytor.repository.UnitRepository;
import java.util.List;
import java.util.Optional;

@Service
public class UnitServiceImpl implements UnitService {

    private UnitRepository unitRepository;

    @Autowired
    public UnitServiceImpl(UnitRepository unitRepository) {
        this.unitRepository = unitRepository;
    }

    @Override
    public List<Unit> findAll() {
        return unitRepository.findAllByOrderByNameAsc();
    }

    @Override
    public Unit findById(int id) {
        Optional<Unit> result = unitRepository.findById(id);
        Unit unit = null;
        if(result.isPresent()) {
            unit = result.get();
        } else {
            throw new RuntimeException("Did not find id unit: " + id);
        }
        return unit;
    }

    @Override
    public List<Unit> searchByName(String name) {
        return unitRepository.findByNameIgnoreCase(name);
    }

    @Override
    public List<Unit> searchById(int id) {
        return unitRepository.findByIdAsc(id);
    }

    @Override
    public List<Unit> searchByWidth (int width) {
        return unitRepository.findByWidth(width);
    }

    @Override
    public List<Unit> searchByLength (int length) {
        return unitRepository.findByLength(length);
    }

    @Override
    public List<Unit> searchDscription(String description) {
        return unitRepository.findByDescription(description);
    }

    @Override
    public void save(Unit unit) {
        unitRepository.save(unit);
    }

    @Override
    public void deleteById(int id) {
        unitRepository.deleteById(id);
    }

}
