package pl.GrzegorzKarolak.spedytor.service.driver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.GrzegorzKarolak.spedytor.entity.Driver;
import pl.GrzegorzKarolak.spedytor.repository.DriverRepository;
import pl.GrzegorzKarolak.spedytor.service.driver.DriverService;

import java.util.List;
import java.util.Optional;

@Service
public class DriverServiceImpl implements DriverService {

    private DriverRepository driverRepository;

    @Autowired
    public DriverServiceImpl(DriverRepository driverRepository) {
        this.driverRepository = driverRepository;
    }

    @Override
    public List<Driver> findAll() {
        return driverRepository.findAllByOrderByLastNameAsc();
    }

    @Override
    public Driver findById(Integer id) {
        Optional<Driver> result = driverRepository.findById(id);
        Driver driver = null;
        if(result.isPresent()) {
            driver = result.get();
        } else {
            throw new RuntimeException("Did not find id driver: " + id);
        }
        return driver;
    }

    @Override
    public List<Driver> findByIdAsc(Integer id) {
        return driverRepository.findByIdAsc(id);
    }

    @Override
    public List<Driver> findByFirstName(String firstName) {
        return driverRepository.findByFirstName(firstName);
    }

    @Override
    public List<Driver> findByLastName(String lastName) {
        return driverRepository.findByLastName(lastName);
    }

    @Override
    public List<Driver> findByPhoneNo(String phoneNo) {
        return driverRepository.findByPhoneNo(phoneNo);
    }

    @Override
    public List<Driver> findByHaveADR(Boolean haveADR) {
        return driverRepository.findByHaveADR(haveADR);
    }

    @Override
    public List<Driver> findByDetails(String details) {
        return driverRepository.findByDetails(details);
    }

    @Override
    public List<Driver> findByCarrierName(String carrier) {
        return driverRepository.findByCarrierName(carrier);
    }

    @Override
    public void save(Driver driver) { driverRepository.save(driver); }

    @Override
    public void deleteById(int id) { driverRepository.deleteById(id); }
}
