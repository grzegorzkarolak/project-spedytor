package pl.GrzegorzKarolak.spedytor.service.driver;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.GrzegorzKarolak.spedytor.entity.Driver;
import pl.GrzegorzKarolak.spedytor.entity.Driver;

import java.util.List;

public interface DriverService {

    List<Driver> findAll();

    Driver findById(Integer id);

    List<Driver> findByIdAsc(Integer id);

    List<Driver> findByFirstName(String firstName);

    List<Driver> findByLastName(String lastName);

    List<Driver> findByPhoneNo(String phoneNo);

    List<Driver> findByHaveADR(Boolean haveADR);

    List<Driver> findByDetails(String details);

    List<Driver> findByCarrierName(String carrier);

    void save(Driver driver);

    void deleteById(int id);



}
