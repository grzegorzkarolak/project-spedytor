package pl.GrzegorzKarolak.spedytor.service.truck;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.GrzegorzKarolak.spedytor.entity.Cargo;
import pl.GrzegorzKarolak.spedytor.entity.Truck;

import java.util.List;

public interface TruckService {

    List<Truck> findAll();

    Truck findById(Integer id);

    List<Truck> findByIdAsc(Integer id);

    List<Truck> findByTruckReg(String truckReg);

    List<Truck> findByLorryReg(String lorryReg);

    List<Truck> findByKind(String kind);

    List<Truck> findByCapacity(Integer capacity);

    List<Truck> findByDetails(String details);

    List<Truck> findByCarrierName(String carrier);

    void save(Truck truck);

    void deleteById(int id);



}
