package pl.GrzegorzKarolak.spedytor.service.truck;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.GrzegorzKarolak.spedytor.entity.Truck;
import pl.GrzegorzKarolak.spedytor.repository.TruckRepository;
import pl.GrzegorzKarolak.spedytor.service.truck.TruckService;

import java.util.List;
import java.util.Optional;

@Service
public class TruckServiceImpl implements TruckService {

    private TruckRepository truckRepository;

    @Autowired
    public TruckServiceImpl(TruckRepository truckRepository) {
        this.truckRepository = truckRepository;
    }

    @Override
    public List<Truck> findAll() {
        return truckRepository.findAllByOrderByTruckRegAsc();
    }

    @Override
    public Truck findById(Integer id) {
        Optional<Truck> result = truckRepository.findById(id);
        Truck truck = null;
        if(result.isPresent()) {
            truck = result.get();
        } else {
            throw new RuntimeException("Did not find id truck: " + id);
        }
        return truck;
    }

    @Override
    public List<Truck> findByIdAsc(Integer id) {
        return truckRepository.findByIdAsc(id);
    }

    @Override
    public List<Truck> findByTruckReg(String truckReg) {
        return truckRepository.findByTruckReg(truckReg);
    }

    @Override
    public List<Truck> findByLorryReg(String lorryReg) {
        return truckRepository.findByLorryReg(lorryReg);
    }

    @Override
    public List<Truck> findByKind(String kind) {
        return truckRepository.findByKind(kind);
    }

    @Override
    public List<Truck> findByCapacity(Integer capacity) {
        return truckRepository.findByCapacity(capacity);
    }

    @Override
    public List<Truck> findByDetails(String details) {
        return truckRepository.findByDetails(details);
    }

    @Override
    public List<Truck> findByCarrierName(String carrier) {
        return truckRepository.findByCarrierName(carrier);
    }

    @Override
    public void save(Truck truck) { truckRepository.save(truck); }

    @Override
    public void deleteById(int id) { truckRepository.deleteById(id); }
}
