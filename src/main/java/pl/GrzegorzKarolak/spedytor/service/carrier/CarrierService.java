package pl.GrzegorzKarolak.spedytor.service.carrier;

import pl.GrzegorzKarolak.spedytor.entity.Carrier;

import java.util.List;

public interface CarrierService {

    List<Carrier> findAll();

    Carrier findById(int theId);

    List<Carrier> searchByName(String name);

    List<Carrier> searchById(int id);

    List<Carrier> searchByAddress(String address);

    List<Carrier> searchByCountry(String country);

    List<Carrier> searchByPerson(String person);

    void save(Carrier Carrier);

    void deleteById(int id);
}
