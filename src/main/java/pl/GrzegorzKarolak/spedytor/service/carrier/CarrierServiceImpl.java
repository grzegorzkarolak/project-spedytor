package pl.GrzegorzKarolak.spedytor.service.carrier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.GrzegorzKarolak.spedytor.entity.Carrier;
import pl.GrzegorzKarolak.spedytor.repository.CarrierRepository;

import java.util.List;
import java.util.Optional;

@Service
public class CarrierServiceImpl implements CarrierService {

    private CarrierRepository carrierRepository;

    @Autowired
    public CarrierServiceImpl(CarrierRepository carrierRepository) {
        this.carrierRepository = carrierRepository;
    }

    @Override
    public List<Carrier> findAll() {
        return carrierRepository.findAllByOrderByNameAsc();
    }

    @Override
    public Carrier findById(int id) {
        Optional<Carrier> result = carrierRepository.findById(id);
        Carrier carrier = null;
        if(result.isPresent()) {
            carrier = result.get();
        } else {
            throw new RuntimeException("Did not find id carrier: " + id);
        }
        return carrier;
    }

    @Override
    public List<Carrier> searchByName(String name) {
        return carrierRepository.findByNameIgnoreCase(name);
    }

    @Override
    public List<Carrier> searchById(int id) {
        return carrierRepository.findByIdAsc(id);
    }

    @Override
    public List<Carrier> searchByAddress(String address) {
        return carrierRepository.findByAddress(address);
    }

    @Override
    public List<Carrier> searchByCountry(String country) {
        return carrierRepository.findByCountry(country);
    }

    @Override
    public List<Carrier> searchByPerson(String person) {
        return carrierRepository.findByPerson(person);
    }

    @Override
    public void save(Carrier carrier) {
        carrierRepository.save(carrier);
    }

    @Override
    public void deleteById(int id) {
        carrierRepository.deleteById(id);
    }

}
