package pl.GrzegorzKarolak.spedytor.service.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.GrzegorzKarolak.spedytor.entity.Customer;
import pl.GrzegorzKarolak.spedytor.repository.CustomerRepository;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerServiceImpl implements CustomerService {

    private CustomerRepository customerRepository;

    @Autowired
    public CustomerServiceImpl (CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public List<Customer> findAll() {
        return customerRepository.findAllByOrderByNameAsc();
    }

    @Override
    public Customer findById(int id) {
        Optional<Customer> result = customerRepository.findById(id);
        Customer customer = null;
        if(result.isPresent()) {
            customer = result.get();
        } else {
            throw new RuntimeException("Did not find id customer: " + id);
        }
        return customer;
    }

    @Override
    public List<Customer> searchByName(String name) {
        return customerRepository.findByNameIgnoreCase(name);
    }

    @Override
    public List<Customer> searchById(int id) {
        return customerRepository.findByIdAsc(id);
    }

    @Override
    public List<Customer> searchByAddress(String address) {
        return customerRepository.findByAddress(address);
    }

    @Override
    public List<Customer> searchByCountry(String country) {
        return customerRepository.findByCountry(country);
    }

    @Override
    public List<Customer> searchByPerson(String person) {
        return customerRepository.findByPerson(person);
    }

    @Override
    public List<Customer> searchByInvoiceDetails(String invoiceDetails) {
        return customerRepository.findByInvoiceDetails(invoiceDetails);
    }

    @Override
    public void save(Customer customer) {
        customerRepository.save(customer);
    }

    @Override
    public void deleteById(int id) {
        customerRepository.deleteById(id);
    }

}
