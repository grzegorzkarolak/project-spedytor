package pl.GrzegorzKarolak.spedytor.service.customer;

import pl.GrzegorzKarolak.spedytor.entity.Customer;

import java.util.List;

public interface CustomerService {

    List<Customer> findAll();

    Customer findById(int theId);

    List<Customer> searchByName(String name);

    List<Customer> searchById(int id);

    List<Customer> searchByAddress(String address);

    List<Customer> searchByCountry(String country);

    List<Customer> searchByPerson(String person);

    List<Customer> searchByInvoiceDetails(String invoiceDetails);

    void save(Customer customer);

    void deleteById(int id);
}
