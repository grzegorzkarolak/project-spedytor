CREATE DATABASE  IF NOT EXISTS `spedytor`;
use `spedytor`;

SET FOREIGN_KEY_CHECKS = 0;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;

CREATE TABLE `address` (
  `id` 					int(11) NOT NULL AUTO_INCREMENT,
  `name` 				varchar(45) DEFAULT NULL,
  `first_address_line` 	varchar(45) DEFAULT NULL,
  `second_address_line` varchar(45) DEFAULT NULL,	
  `city` 				varchar(45) DEFAULT NULL,
  `zip_code` 			varchar(45) DEFAULT NULL,	
  `country` 			varchar(45) DEFAULT NULL,
  `phone_number` 		varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;

CREATE TABLE `customer` (
  `id` 					int(11) NOT NULL AUTO_INCREMENT,
  `name`				varchar(45) DEFAULT NULL,
  `address_id` 			int(11) DEFAULT NULL,
  `invoice_details_id` 	int(11) DEFAULT NULL,
  `contact_details_id` 	int(11) DEFAULT NULL,


  PRIMARY KEY (`id`),
  
  KEY `FK_ADDRESS_idx` (`address_id`),
  CONSTRAINT `FK_ADDRESS` 
  FOREIGN KEY (`address_id`) 
  REFERENCES `address` (`id`) 
  ON DELETE NO ACTION ON UPDATE NO ACTION,
  
  KEY `FK_INVOICE_DETAILS_idx` (`invoice_details_id`),
  CONSTRAINT `FK_INVOICE_DETAILS` 
  FOREIGN KEY (`invoice_details_id`) 
  REFERENCES `invoice_details` (`id`) 
  ON DELETE NO ACTION ON UPDATE NO ACTION,
  
  KEY `FK_CONTACT_DETAILS_idx` (`contact_details_id`),
  CONSTRAINT `FK_CONTACT_DETAILS` 
  FOREIGN KEY (`contact_details_id`) 
  REFERENCES `contact_details` (`id`) 
  ON DELETE NO ACTION ON UPDATE NO ACTION
  
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `invoice_details`
--

DROP TABLE IF EXISTS `invoice_details`;

CREATE TABLE `invoice_details` (
  `id` 					int(11) NOT NULL AUTO_INCREMENT,
  `nip`				varchar(45) DEFAULT NULL,
  `address_id` 			int(11) DEFAULT NULL,

  PRIMARY KEY (`id`),
  
  KEY `FK_ADDRESS_1_idx` (`address_id`),
  CONSTRAINT `FK_ADDRESS_1` 
  FOREIGN KEY(`address_id`)
  REFERENCES `address` (`id`)
  ON DELETE NO ACTION ON UPDATE NO ACTION
  
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `contact_details`
--

DROP TABLE IF EXISTS `contact_details`;

CREATE TABLE `contact_details` (
  `id` 					int(11) NOT NULL AUTO_INCREMENT,
  `name`				varchar(45) DEFAULT NULL,
  `e_mail`				varchar(45) DEFAULT NULL,
  `phone_number`		varchar(45) DEFAULT NULL,

  PRIMARY KEY (`id`)

) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `customer_transport_order`
--

DROP TABLE IF EXISTS `customer_transport_order`;

CREATE TABLE `customer_transport_order` (
  `id` 					int(11) NOT NULL AUTO_INCREMENT,
  `cost`				DOUBLE DEFAULT NULL,
  `payment_due_date` 	int(11) DEFAULT NULL,
  `customer_id`		 	int(11) DEFAULT NULL,
  `fv_issued_id`		int(11) DEFAULT NULL,

  PRIMARY KEY (`id`),
  
  KEY `FK_CUSTOMER_idx` (`customer_id`),
  CONSTRAINT `FK_CUSTOMER` 
  FOREIGN KEY(`customer_id`)
  REFERENCES `customer` (`id`)
  ON DELETE NO ACTION ON UPDATE NO ACTION,
  
  KEY `FK_FV_ISSUED_idx` (`fv_issued_id`),
  CONSTRAINT `FK_FV_ISSUED_idx` 
  FOREIGN KEY(`fv_issued_id`)
  REFERENCES `fv_issued` (`id`)
  ON DELETE NO ACTION ON UPDATE NO ACTION
  
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `carrier`
--

DROP TABLE IF EXISTS `carrier`;

CREATE TABLE `carrier` (
  `id` 					int(11) NOT NULL AUTO_INCREMENT,
  `name`				varchar(45) DEFAULT NULL,
  `default_payment_due_date` 	int(11) DEFAULT 30,
  `address_id`		 	int(11) DEFAULT NULL,
  `contact_details_id`	int(11) DEFAULT NULL,

  PRIMARY KEY (`id`),
  
  KEY `FK_ADDRESS_2_idx` (`address_id`),
  CONSTRAINT `FK_ADDRESS_2` 
  FOREIGN KEY(`address_id`)
  REFERENCES `address` (`id`)
  ON DELETE NO ACTION ON UPDATE NO ACTION,
  
  KEY `FK_CONTACT_DETAILS_idx` (`contact_details_id`),
  CONSTRAINT `FK_CONTACT_DETAILS_idx` 
  FOREIGN KEY(`contact_details_id`)
  REFERENCES `contact_details` (`id`)
  ON DELETE NO ACTION ON UPDATE NO ACTION
  
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `cargo`
--

DROP TABLE IF EXISTS `cargo`;

CREATE TABLE `cargo` (
  `id` 					int(11) NOT NULL AUTO_INCREMENT,
  `description`			varchar(45) DEFAULT NULL,
  `cargo_content_type`	varchar(45) DEFAULT NULL,
  `order_date` 			DATETIME DEFAULT NULL,  
  `pickup_date` 		DATETIME DEFAULT NULL,  
  `delivery_date` 		DATETIME DEFAULT NULL,
  `total_weight`		int(11) DEFAULT NULL,
  `cargo_value`			DOUBLE DEFAULT NULL,
  `shipping_from_id`	int(11) DEFAULT NULL,
  `shipping_to_id`		int(11) DEFAULT NULL,
  `cmr_id`				int(11) DEFAULT NULL,
  `customer_transport_order_id`		int(11) DEFAULT NULL,
  `carrier_transport_order_id`		int(11) DEFAULT NULL,

  PRIMARY KEY (`id`),
  
  KEY `FK_SHIPPING_FROM_idx` (`shipping_from_id`),
  CONSTRAINT `FK_SHIPPING_FROM` 
  FOREIGN KEY(`shipping_from_id`)
  REFERENCES `address` (`id`)
  ON DELETE NO ACTION ON UPDATE NO ACTION,
  
  KEY `FK_SHIPPING_TO_idx` (`shipping_to_id`),
  CONSTRAINT `FK_SHIPPING_TO` 
  FOREIGN KEY(`shipping_to_id`)
  REFERENCES `address` (`id`)
  ON DELETE NO ACTION ON UPDATE NO ACTION,
  
  KEY `FK_CMR_idx` (`cmr_id`),
  CONSTRAINT `FK_CMR` 
  FOREIGN KEY(`cmr_id`)
  REFERENCES `cmr` (`id`)
  ON DELETE NO ACTION ON UPDATE NO ACTION,
  
  KEY `FK_CUSTOMER_TRANSPORT_ORDER_idx` (`customer_transport_order_id`),
  CONSTRAINT `FK_CUSTOMER_TRANSPORT_ORDER` 
  FOREIGN KEY(`customer_transport_order_id`)
  REFERENCES `customer_transport_order` (`id`)
  ON DELETE NO ACTION ON UPDATE NO ACTION,
  
  KEY `FK_CARRIER_TRANSPORT_ORDER_idx` (`carrier_transport_order_id`),
  CONSTRAINT `FK_CARRIER_TRANSPORT_ORDER` 
  FOREIGN KEY(`carrier_transport_order_id`)
  REFERENCES `carrier_transport_order` (`id`)
  ON DELETE NO ACTION ON UPDATE NO ACTION
  
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `fv_issued`
--

DROP TABLE IF EXISTS `fv_issued`;

CREATE TABLE `fv_issued` (
  `id` 					int(11) NOT NULL AUTO_INCREMENT,
  `is_paid`				BOOLEAN DEFAULT false,

  PRIMARY KEY (`id`)

) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `cmr`
--

DROP TABLE IF EXISTS `cmr`;

CREATE TABLE `cmr` (
  `id` 					int(11) NOT NULL AUTO_INCREMENT,

  PRIMARY KEY (`id`)

) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `carrier`
--

DROP TABLE IF EXISTS `parameters_of_cargo`;

CREATE TABLE `parameters_of_cargo` (
  `id` 					int(11) NOT NULL AUTO_INCREMENT,
  `quantity`			int(11) DEFAULT NULL,
  `weight` 				int(11) DEFAULT NULL,
  `height`		 		int(11) DEFAULT NULL,
  `volume`		 		DOUBLE DEFAULT NULL,  
  `unit_id`				int(11) DEFAULT NULL,

  PRIMARY KEY (`id`),
  
  KEY `FK_UNIT_idx` (`unit_id`),
  CONSTRAINT `FK_UNIT` 
  FOREIGN KEY(`unit_id`)
  REFERENCES `unit` (`id`)
  ON DELETE NO ACTION ON UPDATE NO ACTION
  
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `cargos_parameters`
--

DROP TABLE IF EXISTS `cargos_parameters`;

CREATE TABLE `cargos_parameters` (
  `cargo_id` int(11) NOT NULL,
  `parameters_of_cargo_id` int(11) NOT NULL,
  
  PRIMARY KEY (`cargo_id`,`parameters_of_cargo_id`),
  
  KEY `FK_CARGO_idx` (`cargo_id`),
  
  CONSTRAINT `FK_PARAMETERS_OF_CARGO` FOREIGN KEY (`parameters_of_cargo_id`) 
  REFERENCES `parameters_of_cargo` (`id`) 
  ON DELETE NO ACTION ON UPDATE NO ACTION,
  
  CONSTRAINT `FK_CARGO` FOREIGN KEY (`cargo_id`) 
  REFERENCES `cargo` (`id`) 
  ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `carrier_transport_order`
--

DROP TABLE IF EXISTS `carrier_transport_order`;

CREATE TABLE `carrier_transport_order` (
  `id` 					int(11) NOT NULL AUTO_INCREMENT,
  `cost`				DOUBLE DEFAULT NULL,
  `payment_due_date` 	int(11) DEFAULT NULL,
  `carrier_id`		 	int(11) DEFAULT NULL,
  `fv_recived_id`		int(11) DEFAULT NULL,

  PRIMARY KEY (`id`),
  
  KEY `FK_FV_RECIVED_idx` (`fv_recived_id`),
  CONSTRAINT `FK_FV_RECIVED` 
  FOREIGN KEY(`fv_recived_id`)
  REFERENCES `fv_recived` (`id`)
  ON DELETE NO ACTION ON UPDATE NO ACTION
  
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `unit`
--

DROP TABLE IF EXISTS `unit`;

CREATE TABLE `unit` (
  `id` 					int(11) NOT NULL AUTO_INCREMENT,
  `name`				varchar(15) DEFAULT NULL,
  `description` 		varchar(45) DEFAULT NULL,
  `width`		 		int(11) DEFAULT NULL,
  `length`		 		int(11) DEFAULT NULL,

  PRIMARY KEY (`id`)
  
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `fv_recived`
--

DROP TABLE IF EXISTS `fv_recived`;

CREATE TABLE `fv_recived` (
  `id` 					int(11) NOT NULL AUTO_INCREMENT,
  `is_paid`				BOOLEAN DEFAULT false,

  PRIMARY KEY (`id`)

) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `truck`
--

DROP TABLE IF EXISTS `truck`;

CREATE TABLE `truck` (
  `id` 					int(11) NOT NULL AUTO_INCREMENT,
  `capacity`			int(11) DEFAULT NULL,
  `details` 			varchar(45) DEFAULT NULL,
  `kind`		 		varchar(15) DEFAULT NULL,
  `truck_reg`		 	varchar(10) DEFAULT NULL,
  `lorry_reg`			varchar(10) DEFAULT NULL,
  `carrier_id`			int(11) DEFAULT NULL,


  PRIMARY KEY (`id`),
  
  KEY `FK_UNIT1_idx` (`carrier_id`),
  CONSTRAINT `FK_UNIT1` 
  FOREIGN KEY(`carrier_id`)
  REFERENCES `carrier` (`id`)
  ON DELETE NO ACTION ON UPDATE NO ACTION
  
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `driver`
--

DROP TABLE IF EXISTS `driver`;

CREATE TABLE `driver` (
  `id` 					int(11) NOT NULL AUTO_INCREMENT,
  `first_name`			varchar(45) DEFAULT NULL,
  `last_name` 			varchar(45) DEFAULT NULL,
  `phone_no`		 	varchar(15) DEFAULT NULL,
  `have_adr`		 	boolean DEFAULT NULL,
  `details`				varchar(45) DEFAULT NULL,
  `carrier_id`			int(11) DEFAULT NULL,


  PRIMARY KEY (`id`),
  
  KEY `FK_UNIT2_idx` (`carrier_id`),
  CONSTRAINT `FK_UNIT2` 
  FOREIGN KEY(`carrier_id`)
  REFERENCES `carrier` (`id`)
  ON DELETE NO ACTION ON UPDATE NO ACTION
  
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Table structure for table `cargos_parameters`
--

DROP TABLE IF EXISTS `driver_truck`;

CREATE TABLE `driver_truck` (
  `driver_id` int(11) NOT NULL,
  `truck_id` int(11) NOT NULL,
  
  PRIMARY KEY (`truck_id`,`driver_id`),
  
  KEY `FK_TRUCK_idx` (`truck_id`),
  
  CONSTRAINT `FK_TRUCK` FOREIGN KEY (`truck_id`) 
  REFERENCES `truck` (`id`) 
  ON DELETE NO ACTION ON UPDATE NO ACTION,
  
  CONSTRAINT `FK_DRIVER` FOREIGN KEY (`driver_id`) 
  REFERENCES `driver` (`id`) 
  ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



SET FOREIGN_KEY_CHECKS = 1;